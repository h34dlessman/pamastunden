//
//  StundenCell.swift
//  PAMAStunden
//
//  Created by Parsa Amini on 19.05.21.
//

import Foundation
import UIKit
import RealmSwift

protocol StundenCellDelegate : AnyObject {
    func paramChange(_ id: ObjectId, param: String, value: String)
    func delete(_ id: ObjectId)
}

class StundenCell: UICollectionViewCell {
    var id:ObjectId!
    var name:String!
    var price:String!
    var m:String!
    var p:String!
    weak var delegate:StundenCellDelegate?
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var nameLAbel: UILabel!
    @IBOutlet weak var priceText: UITextField!
    @IBOutlet weak var mText: UITextField!
    @IBOutlet weak var pText: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func initialize() {
        priceText.addTarget(self, action: #selector(priceChange), for: .editingDidEnd)
        mText.addTarget(self, action: #selector(mChange), for: .editingDidEnd)
        pText.addTarget(self, action: #selector(pChange), for: .editingDidEnd)
        
        view.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(deleteAction)))
    }
    
    @objc func priceChange(tf: UITextField) {
        if self.price != tf.text {
            self.price = tf.text
            delegate?.paramChange(id, param: "price", value: self.price)
        }
    }
    
    @objc func mChange(tf: UITextField) {
        if self.m != tf.text {
            self.m = tf.text
            delegate?.paramChange(id, param: "m", value: self.m)
        }
    }
    
    @objc func pChange(tf: UITextField) {
        if self.p != tf.text {
            self.p = tf.text
            delegate?.paramChange(id, param: "p", value: self.p)
        }
    }
    
    @objc func deleteAction() {
        delegate!.delete(id)
    }
}
