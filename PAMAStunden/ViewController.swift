//
//  ViewController.swift
//  PAMAStunden
//
//  Created by Parsa Amini on 19.05.21.
//

import UIKit
import Realm
import RealmSwift

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, StundenCellDelegate {
    var mongoDB = App(id:"pamastunden-mulvm")
    var dbKategorie:RLMMongoCollection?
    var dbMisc:RLMMongoCollection?
    @IBOutlet weak var collectionView: UICollectionView!
    
    var kategorien = [Dictionary<String, Any?>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectMongo()
        
        if (!UserDefaults.standard.bool(forKey: "newbinitiation")) {
            UserDefaults.standard.setValue(true, forKey: "newbinitiation")
            p("Press'n'Hold a Cell -> deletion prompt")
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: view.frame.width-5, height: 194.0)
        
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.backgroundColor = .clear
        
        collectionView.alwaysBounceVertical = true;
        
        self.collectionView.register(UINib(nibName: "StundenCell", bundle: nil), forCellWithReuseIdentifier: "stundenCell")
        
        let refresher:UIRefreshControl = UIRefreshControl()
        refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        collectionView.refreshControl = refresher
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgTap)))
        
        navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createKategorieAlert)), animated: false)
    }
    
    @objc func bgTap() {
        view.endEditing(true)
    }
    
    func connectMongo() {
        let credentials = Credentials.userAPIKey("62z4RnXYoJSft2wGHhpRrXHXxU0xkHxQvnaE99uv2XhiDIrn1Y6dz6ZsczXISyMU")
        mongoDB.login(credentials: credentials) { (result) in
            // Remember to dispatch back to the main thread in completion handlers
            // if you want to do anything on the UI.
            DispatchQueue.main.async { [self] in
                switch result {
                case .failure(let error):
                    p("Login failed: \(error)")
                case .success(let user):
                    print("Login as  \(user) succeeded!")
                    
                    let client = mongoDB.currentUser!.mongoClient("PAMAStundenRealm")
                    // Select the database
                    let database = client.database(named: "PAMAStunden")
                    // Select the collection
                    dbKategorie = database.collection(withName: "Kategorie")
                    dbMisc = database.collection(withName: "Misc")
                    
                    loadData()
                    checkForUpdate()
                }
            }
        }
    }
    
    @objc func loadData() {
        dbKategorie!.find(filter: [:], options: .init(0, .init(), .init())) { [self] res in
            switch res {
            case .success(let documents): do {
                print("Kategorien received")
                kategorien = [Dictionary<String, Any>]()
                for doc in documents{
                    let id = (doc["_id"]!?.objectIdValue)
                    let name = doc["name"]!?.stringValue
                    let price = doc["price"]!?.doubleValue
                    let m = doc["m"]!?.doubleValue
                    let p = doc["p"]!?.doubleValue
                    kategorien.append(["_id": id, "name": name, "price": price, "m": m, "p": p ])
                    reloadCollection()
                }
            }
            case .failure(let err): do {
                p("\(err)")
            }
            }
        }
    }
    
    func reloadCollection() {
        DispatchQueue.main.async { [self] in
            collectionView.reloadData()
            collectionView.refreshControl?.endRefreshing()
            
            var mTotal = 0.0, pTotal = 0.0
            for kategori in kategorien {
                mTotal += (kategori["m"] as! Double)
                pTotal += (kategori["p"] as! Double)
            }
            
            title = String(format: "M %.2f | P %.2f", mTotal, pTotal)
        }
    }
    
    @objc func createKategorieAlert() {
        let createAlert = UIAlertController(title: "ᦪ", message: "", preferredStyle: .alert)
        
        createAlert.addTextField { (textField : UITextField!) in
            textField.placeholder = "Name"
        }
        
        createAlert.addTextField { (textField : UITextField!) in
            textField.placeholder = "€/h"
            textField.keyboardType = .numberPad
        }
        
        let submitAction = UIAlertAction(title: "🌵", style: .default) { [self, unowned createAlert] _ in
            let name = createAlert.textFields![0].text
            let price = createAlert.textFields![1].text
            
            createKategorie(name!, price!)
        }
        
        createAlert.addAction(UIAlertAction(title: "close", style: .cancel, handler: { _ in}))
        
        createAlert.addAction(submitAction)
        
        present(createAlert, animated: true) {
        }
    }
    
    func createKategorie(_ name: String, _ price: String) {
        dbKategorie!.insertOne(["name": AnyBSON(name), "price": AnyBSON(Double(price)!), "m": AnyBSON(0.0), "p": AnyBSON(0.0)]) { [self] (res) in
            switch res {
            case .failure(let error):
                p("Call to MongoDB failed: \(error.localizedDescription)")
                return
            case .success(_):
                print("Created")
                loadData()
            }
        }
    }
    
    func paramChange(_ id: ObjectId, param: String, value: String) {
        dbKategorie!.updateOneDocument(filter: ["_id": AnyBSON(id)], update: ["$set": ["\(param)": AnyBSON(Double(value.replacingOccurrences(of: ",", with: "."))!)]]) { [self] (res) in
            switch res {
            case .success(let n): do {
                print("updated! \(n.matchedCount) ")
                loadData()
            }
            case .failure(let err): do {
                p("err linking tags \(err)")
            }
            }
        }
    }
    
    func delete(_ id: ObjectId) {
        let deleteAlert = UIAlertController(title: "DO U WANT TO RIMUW DIS????", message: "(long press == delete)", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { [self, unowned deleteAlert] _ in
            
                dbKategorie!.deleteOneDocument(filter: ["_id": AnyBSON(id)]) { [self] res in
                    switch res {
                    case .success(_): do {
                        print("deleted")
                        loadData()
                    }
                    case .failure(let err): do {
                        p("ERROR DELETING FAILED VIF FROM DB")
                    }
                    }
                }
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "GOD NO", style: .cancel, handler: { _ in}))
        
        present(deleteAlert, animated: true) {}
    }
    
    ////    COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return kategorien.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stundenCell", for: indexPath) as! StundenCell
        
        let o = kategorien[indexPath.row]
        cell.delegate = self
        cell.id = o["_id"] as! ObjectId
        cell.name = o["name"] as! String
        cell.nameLAbel.text = o["name"] as! String
        cell.priceText.text = String(format: "%.0f", o["price"] as! Double)
        cell.price = String(format: "%.0f", o["price"] as! Double)
        cell.mText.text = String(format: "%.2f", o["m"] as! Double)
        cell.m = String(format: "%.2f", o["m"] as! Double)
        cell.pText.text = String(format: "%.2f", o["p"] as! Double)
        cell.p =  String(format: "%.2f", o["p"] as! Double)
        
        
        cell.initialize()
        cell.view.backgroundColor = .systemBackground
        cell.view.layer.cornerRadius = 8.0
        cell.view.layer.borderWidth = 1.0
        cell.view.layer.borderColor = UIColor.clear.cgColor
        cell.view.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.secondaryLabel.cgColor
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 4.5
        cell.layer.shadowOpacity = 0.3
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    }
    
    
    
    ////    MISC
    
    func p(_ s: String) {
        DispatchQueue.main.async { [self] in
            print(s)
            let alert = UIAlertController.init(title: s, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "YAS!", style: .cancel, handler: { _ in }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func checkForUpdate() {
        dbMisc!.find(filter: [:], options: .init(0, .init(), .init())) { [self] res in
            switch res {
            case .success(let documents): do {
                print("Misc received")
                let appVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!
                let version = documents[0]["version"]??.stringValue
                
                if appVersion != version {
                    if Double(appVersion)! < Double( version!)! {
                        p("Update Verfügbar 🐒")
                    } else {
                        self.updateRemoteAppVersion((documents[0]["_id"]!?.objectIdValue)!, appVersion)
                    }
                }
            }
            case .failure(let err): do {
                print("Misc request Error \(err)")
            }
            }
        }
    }
    
    func updateRemoteAppVersion(_ id: ObjectId, _ version: String) {
        dbMisc!.updateOneDocument(filter: ["_id": AnyBSON(id)], update: ["$set": ["version": AnyBSON(version)]]) { (res) in
            switch res {
            case .success(let n): do {
                print("updated appVersion! \(n.matchedCount) ")
            }
            case .failure(let err): do {
                print("ERR updating appVersion!!! \(err)")
            }
            }
        }
    }
}
